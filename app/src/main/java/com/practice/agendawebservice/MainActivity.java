package com.practice.agendawebservice;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.practice.agendawebservice.Objetos.Contactos;
import com.practice.agendawebservice.Objetos.Device;
import com.practice.agendawebservice.Objetos.ProcesosPHP;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    private Button btnGuardar;
    private Button btnListar;
    private Button btnLimpiar;
    private EditText txtNombre;
    private EditText txtDireccion;
    private EditText txtTelefono1;
    private EditText txtTelefono2;
    private EditText txtNotas;
    private CheckBox cbkFavorito;
    private Contactos savedContacto;
    ProcesosPHP php;
    private int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initComponents();
        setEvents();
    }

    public void initComponents(){
        this.php = new ProcesosPHP();
        php.setContext(this);
        txtNombre = (EditText) findViewById(R.id.txtNombre);
        txtTelefono1 = (EditText) findViewById(R.id.txtTelfono1);
        txtTelefono2 = (EditText) findViewById(R.id.txtTelefono2);
        txtDireccion = (EditText) findViewById(R.id.txtDireccion);
        txtNotas = (EditText) findViewById(R.id.txtNotas);
        cbkFavorito = (CheckBox) findViewById(R.id.chkFavorito);
        btnGuardar = (Button) findViewById(R.id.btnGuardar);
        btnListar = (Button) findViewById(R.id.btnListar);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        savedContacto = null;
    }
    public void setEvents(){
        btnGuardar.setOnClickListener(this);
        btnLimpiar.setOnClickListener(this);
        btnListar.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(isNetworkAvailable()){
            switch (v.getId()){
                case R.id.btnGuardar:
                    boolean completo = true;
                    if(txtNombre.getText().toString().equals("")){
                        txtNombre.setError("Introduce el Nombre");
                        completo = false;
                    }
                    if(txtTelefono1.getText().toString().equals("")){
                        txtTelefono1.setError("Introduce el Telefono Principal");
                        completo = false;
                    }
                    if(txtDireccion.getText().toString().equals("")){
                        txtDireccion.setError("Introduce la Direcion");
                        completo = false;
                    }
                    if(completo){
                        Contactos nContacto = new Contactos();

                        nContacto.setNombre(txtNombre.getText().toString());
                        nContacto.setTelefono1(txtTelefono1.getText().toString());
                        nContacto.setTelefono2(txtTelefono2.getText().toString());
                        nContacto.setDomicilio(txtDireccion.getText().toString());
                        nContacto.setNotas(txtNotas.getText().toString());
                        nContacto.setFavorito(cbkFavorito.isChecked() ? 1 : 0);
                        nContacto.setIdMovil(Device.getSecureId(this));
                        if(savedContacto == null){
                            php.insertarContactoWebService(nContacto);
                            Toast.makeText(getApplicationContext(),"Contacto guardado con exito",Toast.LENGTH_SHORT).show();
                            limpiar();
                        }else{
                            php.actualizarContactoWebService(nContacto,id);
                            Toast.makeText(getApplicationContext(),"Contacto Actualizado con exito",Toast.LENGTH_SHORT).show();
                            limpiar();
                        }
                    }
                    break;
                case R.id.btnLimpiar:
                    limpiar();
                    break;
                case R.id.btnListar:
                    Intent intent = new Intent(MainActivity.this,ListaActivity.class);
                    limpiar();
                    startActivityForResult(intent, 0);
                    break;
            }
        }else{
            Toast.makeText(getApplicationContext(),"Se necesita tener conexion a internet",Toast.LENGTH_SHORT).show();
        }
    }

    public boolean isNetworkAvailable(){
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }

    public void limpiar(){
        savedContacto = null;
        txtNombre.setText("");
        txtTelefono1.setText("");
        txtTelefono2.setText("");
        txtDireccion.setText("");
        txtNotas.setText("");
        cbkFavorito.setChecked(false);
        id = 0;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent){
        super.onActivityResult(requestCode,resultCode,intent);
        if(intent != null){
            Bundle bundle = intent.getExtras();
            if (Activity.RESULT_OK == resultCode){
                Contactos contacto = (Contactos) bundle.getSerializable("contacto");
                savedContacto = contacto;
                id = contacto.get_ID();
                txtNombre.setText(contacto.getNombre());
                txtTelefono1.setText(contacto.getTelefono1());
                txtTelefono2.setText(contacto.getTelefono2());
                txtDireccion.setText(contacto.getDomicilio());
                txtNotas.setText(contacto.getNotas());
                if(contacto.getFavorito() > 0){
                    cbkFavorito.setChecked(true);
                }
            }else{
                limpiar();
            }
        }
    }
}
